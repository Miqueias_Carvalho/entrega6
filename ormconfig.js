const configEnv = {
  type: "postgres",
  port: 5432,
  logging: false,
  synchronize: true,
  host: process.env.PG_HOST,
  database: process.env.POSTGRES_DB,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  entities: ["./src/entity/**/*.ts"],
  migrations: ["./src/database/migrations/*.ts"],
  cli: {
    entitiesDir: "./src/entity",
    migrationsDir: "./src/database/migrations",
  },
};
const configProd = {
  type: "postgres",
  url: process.env.DATABASE_URL,
  synchronize: false,
  logging: false,
  entities: ["./dist/src/entity/**/*.js"],
  migrations: ["./dist/src/database/migrations/*js"],
  cli: {
    migrationsDir: "./dist/src/database/migrations",
    entitiesDir: "./dist/src/entity",
  },
  ssl:
    process.env.NODE_ENV === "production"
      ? { rejectUnauthorized: false }
      : false,
};

const configTest = {
  type: "sqlite",
  database: ":memory:",
  entities: ["./src/entity/**/*.ts"],
  synchronize: true,
};

let exportModule = undefined;

if (process.env.NODE_ENV === "production") {
  exportModule = configProd;
} else if (process.env.NODE_ENV === "test") {
  exportModule = configTest;
} else {
  exportModule = configEnv;
}

module.exports = exportModule;
