import { Express } from "express";
import userRouter from "./userRoutes";
import emailRouter from "./emailRoutes";
import productRouter from "./productRoutes";
import cartRouter from "./cartRoutes";
import purchaseRouter from "./purchaseRoutes";

export const initializeRouter = (app: Express) => {
  app.use(userRouter);
  app.use(productRouter);
  app.use(cartRouter);
  app.use(purchaseRouter);
  app.use(emailRouter);
};
