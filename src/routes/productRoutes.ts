import { Router } from "express";

import { productSchema } from "../schemas/productSchema";

import { validate } from "../middlewares/validation.middleware";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { isAuthorizated } from "../middlewares/authorization.middleware";
import { isAdmUser } from "../middlewares/adm.middleware";

import ListOneProductController from "../controllers/product/listOneProduct.controller";
import ListProductsController from "../controllers/product/listProducts.controller";
import CreateProductController from "../controllers/product/createProduct.controller";

const productRouter = Router();

const product = new ListOneProductController();
const productsList = new ListProductsController();
const createProduct = new CreateProductController();

/**
 * @openapi
 * /product:
 *  post:
 *     tags:
 *     - Cria produto
 *     summary: Cadastra um único produto
 *     description: Apenas usuário do tipo administrador pode realizar essa ação
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ProductSchema'
 *     responses:
 *       201:
 *         description: created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ProductResponseSchema'
 *       409:
 *         description: Conflict
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

productRouter.post(
  "/product",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  validate(productSchema),
  createProduct.handle
);

/**
 * @openapi
 * /product:
 *  get:
 *     tags:
 *     - Listar produtos cadastrados
 *     description: Qualquer usuário pode realizar essa ação
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ProductListResponseSchema'
 *     security:
 *       []
 */

productRouter.get("/product", productsList.handle);

/**
 * @openapi
 * /product/{productId}:
 *  get:
 *     tags:
 *     - Pegar produto
 *     summary: Seleciona um único produto pelo id
 *     description: Qualquer usuário pode realizar essa ação
 *     parameters:
 *      - name: productId
 *        in: path
 *        description: id do produto
 *        required: true
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ProductResponseSchema'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *     security:
 *       []
 *
 */

productRouter.get("/product/:id", product.handle);

export default productRouter;
