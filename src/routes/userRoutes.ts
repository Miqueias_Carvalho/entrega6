import { Router } from "express";

import { UserSchema } from "../schemas/userSchema";
import { LoginSchema } from "../schemas/loginSchema";
import { updatePasswordSchema } from "../schemas/changePasswordSchema";

import { validate } from "../middlewares/validation.middleware";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { isAuthorizated } from "../middlewares/authorization.middleware";
import { isAdmUser } from "../middlewares/adm.middleware";

import CreateUserController from "../controllers/user/createUser.controller";
import ListUsersController from "../controllers/user/listUsers.controller";
import UpdateUserController from "../controllers/user/updateUser.controller";
import UserProfileController from "../controllers/user/profile.controller";
import LoginController from "../controllers/user/login.controller";

const userRouter = Router();

const createUser = new CreateUserController();
const listUsers = new ListUsersController();
const updateUse = new UpdateUserController();
const login = new LoginController();
const profile = new UserProfileController();

/**
 * @openapi
 * /user:
 *  post:
 *     tags:
 *     - Cria Usuário
 *     summary: registra um usuário
 *     description: Um usuário pode ser do tipo cliente ou do tipo administrador
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateUserSchema'
 *     responses:
 *       201:
 *         descripition: created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreateUserResponseSchema'
 *       409:
 *         description: Conflict
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *     security:
 *       []
 *
 */

userRouter.post("/user", validate(UserSchema), createUser.handle);

/**
 * @openapi
 * /login:
 *  post:
 *     tags:
 *     - Login
 *     summary: login de um usuário
 *     description: Gera um token de autenticação
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LoginSchema'
 *     responses:
 *       201:
 *         description: created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LoginResponseSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *     security:
 *       []
 *
 */

userRouter.post("/login", validate(LoginSchema), login.handle);
/**
 * @openapi
 * /user:
 *  get:
 *     tags:
 *     - Lista Usuários
 *     description: Listando todos os usuários registrados. Apenas os administradores podem fazer essa ação
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/AllUserResponseSchema'
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */
userRouter.get(
  "/user",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  listUsers.handle
);

/**
 * @openapi
 * /user/{userUUID}:
 *  get:
 *     tags:
 *     - Pegar Usuário
 *     summary: Seleciona um determinado usuário pelo uuid
 *     description: Usuários do tipo cliente são restritos as suas próprias informações mas um adm pode selecionar qualquer usuário do sistema
 *     parameters:
 *      - name: userUUID
 *        in: path
 *        description: uuid do usuário
 *        required: true
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CreateUserResponseSchema'
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

userRouter.get("/user/:uuid", isAuthenticated, isAdmUser, profile.handle);

/**
 * @openapi
 * /alterar_senha:
 *  post:
 *     tags:
 *     - Mudança de senha
 *     summary: muda a senha um usuário
 *     description: A partir do código validador que o usuário recebe em seu email, o usuário pode alterar a senha mandando no body da requisição o codigo, a nova senha e sua confirmação.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ChangePasswordSchema'
 *     responses:
 *       200:
 *         description: created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ChangePasswordResponseSchema'
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 */

userRouter.post(
  "/alterar_senha",
  isAuthenticated,
  validate(updatePasswordSchema),
  updateUse.changePassword
);
export default userRouter;
