import { Router } from "express";

import { isAuthenticated } from "../middlewares/authentication.middleware";
import { isAuthorizated } from "../middlewares/authorization.middleware";
import { isAdmUser } from "../middlewares/adm.middleware";

import FinishBuyController from "../controllers/purchase/finishBuy.controller";
import ListBuyController from "../controllers/purchase/listBuys.controller";
import SelecBuyController from "../controllers/purchase/selectBuy.controller";

const finishBuy = new FinishBuyController();
const listBuy = new ListBuyController();
const selectBuy = new SelecBuyController();

const purchaseRouter = Router();

/**
 * @openapi
 * /buy:
 *  post:
 *     tags:
 *     - Fazer compra
 *     summary: Realiza compra dos produtos do carrinho
 *     description: Apenas o dono do carrinho pode finalizar uma compra e após a sua finalização um email com os dados da compra deve ser disparado para o usuário.
 *     requestBody:
 *       required: false
 *     responses:
 *       201:
 *         description: created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/BuyResponseSchema'
 *       404:
 *         description: Not Found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

purchaseRouter.post("/buy", isAuthenticated, finishBuy.handle);

/**
 * @openapi
 * /buy/{buyId}:
 *  get:
 *     tags:
 *     - Selecionar compra
 *     summary: Seleciona uma compra já realizada
 *     description: Apenas o usuário dono da compra e o administrador pode realizar essa ação
 *     parameters:
 *      - name: buyId
 *        in: path
 *        description: id da compra
 *        required: true
 *     requestBody:
 *       required: false
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/BuyResponseSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

purchaseRouter.get(
  "/buy/:id",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  selectBuy.handle
);

/**
 * @openapi
 * /buy:
 *  get:
 *     tags:
 *     - Listar compras
 *     summary: Seleciona todas as compras já realizadas
 *     description: Apenas os administradores podem realizar essa ação
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/BuyListResponseSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 */

purchaseRouter.get(
  "/buy",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  listBuy.handle
);

export default purchaseRouter;
