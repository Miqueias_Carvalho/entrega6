import { Router } from "express";

import { EmailSchema } from "../schemas/emailSchema";

import { validate } from "../middlewares/validation.middleware";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { isAuthorizated } from "../middlewares/authorization.middleware";
import { isAdmUser } from "../middlewares/adm.middleware";

import EmailController from "../controllers/user/email.controller";

const emailRouter = Router();

const sendEmail = new EmailController();

/**
 * @openapi
 * /email:
 *  post:
 *     tags:
 *     - enviar email
 *     summary: Envia um email qualquer para um determinado usuário cadastrado
 *     description: Apenas usuário do tipo administrador pode realizar essa ação
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/EmailSchema'
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/EmailResponseSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

emailRouter.post(
  "/email",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  validate(EmailSchema),
  sendEmail.handle
);

/**
 * @openapi
 * /recuperar:
 *  post:
 *     tags:
 *     - Recuperar senha
 *     summary: receber código para mudança de senha
 *     description: Envia um código de validação para o email do usuário
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/RecuperarResponseSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 */

emailRouter.post("/recuperar", isAuthenticated, sendEmail.handlePasswordCode);

export default emailRouter;
