import { Router } from "express";

import { cartSchema } from "../schemas/cartSchema";
import { removeProductSchema } from "../schemas/removeProductSchema";

import { validate } from "../middlewares/validation.middleware";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { isAuthorizated } from "../middlewares/authorization.middleware";
import { isAdmUser } from "../middlewares/adm.middleware";

import AddItemCartController from "../controllers/cart/addProduct.controller";
import ListCartController from "../controllers/cart/listCarts.controller";
import SelectCartController from "../controllers/cart/selectCart.Controller";
import RemoveProductController from "../controllers/cart/removeProduct.controller";

const cartRouter = Router();

const addItem = new AddItemCartController();
const getCart = new SelectCartController();
const listcarts = new ListCartController();
const removeProduct = new RemoveProductController();

/**
 * @openapi
 * /cart:
 *  post:
 *     tags:
 *     - adicionar no carrinho
 *     summary: adiciona produto ao carrinho
 *     description: Apenas usuários logados devem adicionar o produto no carrinho
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CartSchema'
 *     responses:
 *       201:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/CartResponseSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

cartRouter.post("/cart", isAuthenticated, validate(cartSchema), addItem.handle);

/**
 * @openapi
 * /cart/{productId}:
 *  delete:
 *     tags:
 *     - remover do carrinho
 *     summary: remove  um produto do carrinho
 *     description: Apenas o usuário dono do carrinho e o administrador pode realizar essa ação
 *     parameters:
 *      - name: productId
 *        in: path
 *        description: id de um produto no carrinho
 *        required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RemoveProductFromCartSchema'
 *     responses:
 *       201:
 *         description: retorna o carrinho
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/SingleCartResponseSchema'
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

cartRouter.delete(
  "/cart/:product_id",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  validate(removeProductSchema),
  removeProduct.handle
);

/**
 * @openapi
 * /cart:
 *  get:
 *     tags:
 *     - Listar carrinhos
 *     summary: Seleciona todos os carrinhos
 *     description: Apenas os administradores podem realizar essa ação
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ListCartsResponseSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 */

cartRouter.get(
  "/cart",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  listcarts.handle
);

/**
 * @openapi
 * /cart/{cartId}:
 *  get:
 *     tags:
 *     - Pegar carrinho
 *     summary: Seleciona um único carrinho pelo id
 *     description: Usuários do tipo cliente são restritos as suas próprias informações.Um cliente não pode selecionar o carrinho de outro usuário, mas um administrador pode selecionar qualquer carrinho do sistema
 *     parameters:
 *      - name: cartId
 *        in: path
 *        description: id do carrinho
 *        required: true
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/SingleCartResponseSchema'
 *       404:
 *         description: not found
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *       401:
 *         description: unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorSchema'
 *
 */

cartRouter.get(
  "/cart/:id",
  isAuthenticated,
  isAdmUser,
  isAuthorizated,
  getCart.handle
);

export default cartRouter;
