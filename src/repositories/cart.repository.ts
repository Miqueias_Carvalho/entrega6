import { EntityRepository, Repository } from "typeorm";
import { Cart } from "../entity/cart.entity";

@EntityRepository(Cart)
class CartRepository extends Repository<Cart> {}

export { CartRepository };
