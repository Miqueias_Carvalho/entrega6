import { EntityRepository, Repository } from "typeorm";
import { Purchase } from "../entity/purchase.entity";

@EntityRepository(Purchase)
class PurchaseRepository extends Repository<Purchase> {}

export { PurchaseRepository };
