import { Response } from "express";

export class ErrorHandler {
  public readonly statusCode: number;
  public readonly message: string;

  constructor(statusCode: number, message: any) {
    this.statusCode = statusCode;
    this.message = message;
  }
}

export const handleError = (err: Error, res: Response) => {
  if (err instanceof ErrorHandler) {
    return res.status(err.statusCode).json({
      status: "error",
      statusCode: err.statusCode,
      message: err.message,
    });
  }

  return res.status(500).json({
    status: "error",
    message: "Internal server error",
  });
};
