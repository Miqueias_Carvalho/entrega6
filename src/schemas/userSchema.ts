import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     CreateUserSchema:
 *        type: object
 *        required:
 *          - name
 *          - email
 *          - password
 *          - isAdm
 *        properties:
 *          email:
 *            type: string
 *            default: testando@mail.com
 *          name:
 *            type: string
 *            default: Testador
 *          password:
 *            type: string
 *            default: abcd
 *          isAdm:
 *            type: boolean
 *            default: true
 *     CreateUserResponseSchema:
 *        type: object
 *        properties:
 *          email:
 *            type: string
 *          name:
 *            type: string
 *          code:
 *            type: string
 *          uuid:
 *            type: string
 *          isAdm:
 *            type: boolean
 *          createdOn:
 *            type: string
 *          updatedOn:
 *            type: string
 *     AllUserResponseSchema:
 *        type: array
 *        items:
 *          type: object
 *          properties:
 *            email:
 *              type: string
 *            name:
 *              type: string
 *            uuid:
 *              type: string
 *            isAdm:
 *              type: boolean
 *            createdOn:
 *              type: string
 *            updatedOn:
 *              type: string
 *     ErrorSchema:
 *        type: object
 *        properties:
 *          status:
 *            type: string
 *          statusCode:
 *            type: number
 *          message:
 *            type: string
 */

export const UserSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  isAdm: yup.boolean().required(),
  password: yup.string().min(4).required(),
});
