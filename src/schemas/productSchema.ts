import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     ProductSchema:
 *        type: object
 *        required:
 *          - name
 *          - price
 *        properties:
 *          name:
 *            type: string
 *            default: produto A
 *          price:
 *            type: number
 *            default: 1.99
 *     ProductResponseSchema:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          price:
 *            type: number
 *          id:
 *            type: number
 *     ProductListResponseSchema:
 *        type: array
 *        items:
 *          type: object
 *          properties:
 *            name:
 *              type: string
 *            price:
 *              type: number
 *            id:
 *              type: number
 */

export const productSchema = yup.object().shape({
  name: yup.string().required(),
  price: yup.number().required(),
});
