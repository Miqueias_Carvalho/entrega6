import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     LoginSchema:
 *        type: object
 *        required:
 *          - password
 *          - email
 *        properties:
 *          email:
 *            type: string
 *            default: testando@mail.com
 *          password:
 *            type: string
 *            default: abcd
 *     LoginResponseSchema:
 *        type: object
 *        properties:
 *          token:
 *            type: string
 */

export const LoginSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().min(4).required(),
});
