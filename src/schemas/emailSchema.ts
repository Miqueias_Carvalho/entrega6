import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     EmailSchema:
 *        type: object
 *        required:
 *          - to
 *          - subject
 *          - text
 *        properties:
 *          to:
 *            type: string
 *            default: testando@mail.com
 *          subject:
 *            type: string
 *            default: enviar email
 *          text:
 *            type: string
 *            default: fazendo teste de envio de email
 *     EmailResponseSchema:
 *        type: object
 *        properties:
 *          message:
 *            type: string
 *     RecuperarResponseSchema:
 *        type: object
 *        properties:
 *          message:
 *            type: string
 *          code:
 *            type: string
 */

export const EmailSchema = yup.object().shape({
  to: yup.string().email().required(),
  subject: yup.string().required(),
  text: yup.string().required(),
});
