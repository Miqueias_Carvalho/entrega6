import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     ChangePasswordSchema:
 *        type: object
 *        required:
 *          - password
 *          - code
 *          - confirm
 *        properties:
 *          code:
 *            type: string
 *            default: 2ee69fa5-7e8b-48e9-96e9-330cd5f2a97e
 *          confirm:
 *            type: boolean
 *            default: true
 *          password:
 *            type: string
 *            default: abcd
 *     ChangePasswordResponseSchema:
 *        type: object
 *        properties:
 *          password:
 *            type: string
 */

export const updatePasswordSchema = yup.object().shape({
  password: yup.string().min(4).required(),
  code: yup.string().required(),
  confirm: yup.boolean().required(),
});
