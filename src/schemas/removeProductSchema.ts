import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     RemoveProductFromCartSchema:
 *        type: object
 *        required:
 *          - cartId
 *        properties:
 *          cartId:
 *            type: number
 *            default: 1
 */

export const removeProductSchema = yup.object().shape({
  cartId: yup.number().required(),
});
