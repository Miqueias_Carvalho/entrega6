import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     UpdateSchema:
 *        type: object
 *        required:
 *          - name
 *          - email
 *        properties:
 *          name:
 *            type: string
 *            default: novo nome
 *          confirm:
 *            type: string
 *            default: novo@mail.com
 */

export const UpdateSchema = yup.object().shape({
  name: yup.string(),
  email: yup.string().email(),
});
