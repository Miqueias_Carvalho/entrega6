import * as yup from "yup";

/**
 * @openapi
 * components:
 *   schemas:
 *     CartSchema:
 *        type: object
 *        required:
 *          - id
 *          - name
 *          - price
 *        properties:
 *          id:
 *            type: number
 *            default: 4
 *          name:
 *            type: string
 *            default: produto A
 *          price:
 *            type: number
 *            default: 1.99
 *     CartResponseSchema:
 *        type: object
 *        properties:
 *          cart:
 *            properties:
 *              id:
 *                type: number
 *              products:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    name:
 *                      type: string
 *                    price:
 *                      type: number
 *                    id:
 *                      type: number
 *              user:
 *                type: object
 *                properties:
 *                  email:
 *                    type: string
 *                  name:
 *                    type: string
 *                  uuid:
 *                    type: string
 *                  isAdm:
 *                    type: boolean
 *                  createdOn:
 *                    type: string
 *                  updatedOn:
 *                    type: string
 *          preco_total:
 *            type: number
 *     SingleCartResponseSchema:
 *        type: object
 *        properties:
 *          id:
 *            type: number
 *          products:
 *            type: array
 *            items:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                price:
 *                  type: number
 *                id:
 *                  type: number
 *          user:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              name:
 *                type: string
 *              uuid:
 *                type: string
 *              isAdm:
 *                type: boolean
 *              createdOn:
 *                type: string
 *              updatedOn:
 *                type: string
 *     ListCartsResponseSchema:
 *        type: array
 *        items:
 *          type: object
 *          properties:
 *            id:
 *              type: number
 *            products:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                  price:
 *                    type: number
 *                  id:
 *                    type: number
 *            user:
 *              type: object
 *              properties:
 *                email:
 *                  type: string
 *                name:
 *                  type: string
 *                uuid:
 *                  type: string
 *                isAdm:
 *                  type: boolean
 *                createdOn:
 *                  type: string
 *                updatedOn:
 *                  type: string
 *
 *     BuyResponseSchema:
 *        type: object
 *        properties:
 *          id:
 *            type: number
 *          products:
 *            type: array
 *            items:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                price:
 *                  type: number
 *                id:
 *                  type: number
 *          user:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              name:
 *                type: string
 *              uuid:
 *                type: string
 *              isAdm:
 *                type: boolean
 *              createdOn:
 *                type: string
 *              updatedOn:
 *                type: string
 *          purchasedOn:
 *            type: string
 *     BuyListResponseSchema:
 *        type: array
 *        items:
 *          type: object
 *          properties:
 *            id:
 *              type: number
 *            purchasedOn:
 *              type: string
 *            products:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                  price:
 *                    type: number
 *                  id:
 *                    type: number
 *            user:
 *              type: object
 *              properties:
 *                email:
 *                  type: string
 *                name:
 *                  type: string
 *                uuid:
 *                  type: string
 *                isAdm:
 *                  type: boolean
 *                createdOn:
 *                  type: string
 *                updatedOn:
 *                  type: string
 */

export const cartSchema = yup.object().shape({
  id: yup.number().required(),
  name: yup.string().required(),
  price: yup.number().required(),
});
