import { NextFunction, Request, Response } from "express";
import { ErrorHandler } from "../utils/error";

export const isAuthorizated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.user?.isAdm) {
    return next();
  }
  const { uuid } = req.params;
  if (req.user?.uuid !== uuid) {
    return next(new ErrorHandler(401, "Missing admin permissions"));
  }
  next()
};
