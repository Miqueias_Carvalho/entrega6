import { NextFunction, Request, Response } from "express";
import * as yup from "yup";
import { ErrorHandler } from "../utils/error";

export const validate =
  (schema: yup.AnyObjectSchema) =>
  async (req: Request, res: Response, next: NextFunction) => {
    const body = req.body;

    try {
      await schema.validate(body, { abortEarly: false, stripUnknown: true });
      return next();
    } catch (e) {
      return next(
        new ErrorHandler(400, { [(e as any).name]: (e as any).errors })
      );
    }
  };
