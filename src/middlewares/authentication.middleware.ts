import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { ErrorHandler } from "../utils/error";

export const isAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization) {
    return next(new ErrorHandler(401, "Missing authorization headers"));
  } else {
    const token = req.headers.authorization.split(" ")[1];

    jwt.verify(
      token as string,
      process.env.JWT_SECRET as string,
      (err: any, decoded: any) => {
        if (err) {
          return next(new ErrorHandler(401, "Invalid Token"));
        }
        const userUuid = decoded.uuid;
        const email = decoded.email;
        req.user = { uuid: userUuid, email: email };
      }
    );
  }
  next();
};
