import { NextFunction, Request, Response } from "express";
import { UserRepository } from "../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../utils/error";

export const isAdmUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const uuid = req.user?.uuid;
  const userRepository = getCustomRepository(UserRepository);
  const user = await userRepository.findOne({ uuid });

  if (!user) {
    return next(new ErrorHandler(404, "User not Found"));
  }
  req.user = { ...req.user, isAdm: user.isAdm };
  next();
};
