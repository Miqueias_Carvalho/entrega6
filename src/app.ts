import express from "express";
import { handleError } from "./utils/error";
import { NextFunction, Response, Request } from "express";
import { initializeRouter } from "./routes";
import dotenv from "dotenv";
import swaggerDocs from "./utils/swagger";

dotenv.config();

const app = express();
app.use(express.json());

initializeRouter(app);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  handleError(err, res);
});

swaggerDocs(app);

export default app;
