import { createConnection, getConnection } from "typeorm";
import request from "supertest";
import app from "../../app";

describe("Testing user routes", () => {
  beforeAll(async () => {
    await createConnection();
  });

  afterAll(async () => {
    const defaultConnection = getConnection("default");
    await defaultConnection.close();
  });

  let token = "";
  let uuid = "";
  let code = "";
  let productId = "";
  let cartId = 0;
  let purchaseId = "";

  it("Should be able to create a new user A", async () => {
    const response = await request(app).post("/user").send({
      name: "UserA",
      email: "usera@mail.com",
      password: "1234",
      isAdm: true,
    });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("uuid");

    uuid = response.body.uuid;
  });

  it("Should be able to create a new user B", async () => {
    const response = await request(app).post("/user").send({
      name: "UserB",
      email: "userb@mail.com",
      password: "1234",
      isAdm: false,
    });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("uuid");
  });

  it("Should be able to login with the created user A", async () => {
    const response = await request(app).post("/login").send({
      email: "usera@mail.com",
      password: "1234",
    });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("token");

    token = response.body.token;
  });

  it("Should be able to get one user informations", async () => {
    const response = await request(app)
      .get(`/user/${uuid}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("name");
    expect(response.body.name).toBe("UserA");
  });

  it("Should be able to list users", async () => {
    const response = await request(app)
      .get(`/user`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("map");
    expect(response.body).toHaveLength(2);
  });

  it("Should be able to sent a email", async () => {
    const response = await request(app)
      .post("/email")
      .set({ Authorization: `Bearer ${token}` })
      .send({
        to: "usera@mail.com",
        subject: "testando email",
        text: "O teste foi realizado realizada com sucesso!",
      });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("message");
  });
  it("Should be able to sent a email to change the password", async () => {
    const response = await request(app)
      .post("/recuperar")
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("message");
    expect(response.body).toHaveProperty("code");

    code = response.body.code;
  });
  it("Should be able to change the pawword", async () => {
    const response = await request(app)
      .post("/alterar_senha")
      .set({ Authorization: `Bearer ${token}` })
      .send({
        code: code,
        password: "abcd",
        confirm: true,
      });
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("password");
    expect(response.body.password).toBe("abcd");
  });
  it("Should unable to log with the old password", async () => {
    const response = await request(app).post("/login").send({
      email: "usera@mail.com",
      password: "1234",
    });

    expect(response.status).toBe(401);
    expect(response.body).toHaveProperty("status");
    expect(response.body.message).toBe("User and password missmatch.");
  });

  it("Should create a product", async () => {
    const response = await request(app)
      .post("/product")
      .set({ Authorization: `Bearer ${token}` })
      .send({
        name: "produto1",
        price: 233.2,
      });
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("id");

    productId = response.body.id;
  });
  it("Should be able to get one product", async () => {
    const response = await request(app).get(`/product/${productId}`);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("name");
  });
  it("Should list the products", async () => {
    const response = await request(app).get(`/product`);

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("map");
    expect(response.body).toHaveLength(1);
  });

  it("Should add produtc in the cart", async () => {
    const response = await request(app)
      .post(`/cart`)
      .set({ Authorization: `Bearer ${token}` })
      .send({ id: productId, name: "produto1", price: 233.2 });

    cartId = response.body.cart.id;

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("preco_total");
    expect(response.body.cart).toHaveProperty("products");
    expect(response.body.cart).toHaveProperty("user");
  });

  it("Should select one cart", async () => {
    const response = await request(app)
      .get(`/cart/${cartId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("user");
    expect(response.body).toHaveProperty("products");
    expect(response.body.products).toHaveLength(1);
  });

  it("Should list the carts", async () => {
    const response = await request(app)
      .get(`/cart`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("map");
    expect(response.body).toHaveLength(1);
  });

  it("Should remove a product from the carts", async () => {
    const response = await request(app)
      .delete(`/cart/${productId}`)
      .set({ Authorization: `Bearer ${token}` })
      .send({ cartId: cartId });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("products");
    expect(response.body.products).toHaveLength(0);
  });

  it("Should add produtc in the cart", async () => {
    const response = await request(app)
      .post(`/cart`)
      .set({ Authorization: `Bearer ${token}` })
      .send({ id: productId, name: "produto1", price: 233.2 });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("preco_total");
    expect(response.body.cart).toHaveProperty("products");
    expect(response.body.cart).toHaveProperty("user");
  });

  it("Should finish a purchase", async () => {
    const response = await request(app)
      .post(`/buy`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("purchasedOn");
    expect(response.body).toHaveProperty("products");
    expect(response.body).toHaveProperty("id");

    purchaseId = response.body.id;
  });

  it("Should select a purchase", async () => {
    const response = await request(app)
      .get(`/buy/${purchaseId}`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("purchasedOn");
    expect(response.body).toHaveProperty("products");
    expect(response.body).toHaveProperty("user");
  });

  it("Should list all purchases", async () => {
    const response = await request(app)
      .get(`/buy`)
      .set({ Authorization: `Bearer ${token}` });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("map");
    expect(response.body).toHaveLength(1);
    expect(response.body[0]).toHaveProperty("user");
  });
});
