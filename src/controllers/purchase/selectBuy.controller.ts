import { Request, Response, NextFunction } from "express";
import SelectBuyService from "../../services/purchase/selectbuy.service";

export default class SelecBuyController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid as string;
      const isAdm = req.user?.isAdm as boolean;

      const id = parseInt(req.params.id);

      const buyService = new SelectBuyService();
      const buyResponse = await buyService.execute(id, uuid, isAdm);

      return res.status(200).json(buyResponse);
    } catch (err) {
      next(err);
    }
  }
}
