import { Request, Response, NextFunction } from "express";
import FinishBuyService from "../../services/purchase/finishBuy.service";
import {
  transport,
  mailTemplateOptions,
} from "../../services/email/email.service";
import { protectedReturn } from "../../utils/utils";

export default class FinishBuyController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid as string;

      const buyService = new FinishBuyService();
      const buyResponse = await buyService.execute(uuid);

      const listProducts = buyResponse.products.map((item) => {
        let price = item.price.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        });

        return `${item.name} : ${price}`;
      });

      const message = listProducts.join();
      const from = "no-repply@mail.com";
      const subject = "Suas compras foram realizadas";

      const options = mailTemplateOptions(
        from,
        buyResponse.user.email,
        subject,
        "buy",
        {
          name: buyResponse.user.name,
          message: message,
          purchaseOn: buyResponse.purchasedOn,
        }
      );

      transport.sendMail(options, function (err, info) {
        if (err) {
          return next(err);
        }
        // else {
        //   console.log(info);
        // }
      });

      const buyReturn = protectedReturn(buyResponse);

      return res.status(201).json(buyReturn);
    } catch (err) {
      next(err);
    }
  }
}
