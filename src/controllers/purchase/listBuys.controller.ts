import { Request, Response, NextFunction } from "express";
import ListBuyService from "../../services/purchase/listbuys.service";
import { ErrorHandler } from "../../utils/error";

export default class ListBuyController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const isAdm = req.user?.isAdm as boolean;
      if (!isAdm) {
        throw new ErrorHandler(401, "Unauthorized");
      }

      const buyService = new ListBuyService();
      const buyResponse = await buyService.execute();

      return res.status(200).json(buyResponse);
    } catch (err) {
      next(err);
    }
  }
}
