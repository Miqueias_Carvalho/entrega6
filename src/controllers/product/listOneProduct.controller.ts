import { NextFunction, Response, Request } from "express";
import OneProductService from "../../services/product/listOneProduct.service";

export default class ListOneProductController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const id = parseInt(req.params.id);

      const productService = new OneProductService();
      const product = await productService.execute(id);

      return res.status(200).json(product);
    } catch (e) {
      next(e);
    }
  }
}
