import { Request, Response, NextFunction } from "express";
// import { ErrorHandler } from "../../utils/error";
import ListProductsService from "../../services/product/listProducts.service";

export default class ListProductsController {
  async handle(req: Request, res: Response, next: NextFunction) {
    const listProductsService = new ListProductsService();

    const products = await listProductsService.execute();

    return res.status(200).json(products);
  }
}
