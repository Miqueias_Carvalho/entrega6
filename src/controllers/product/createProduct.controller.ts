import { Request, Response, NextFunction } from "express";
import CreateProductService from "../../services/product/createProduct.service";

export default class CreateProductController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const createdProduct = new CreateProductService();
      const product = await createdProduct.execute(req.body);

      return res.status(201).json(product);
    } catch (err) {
      next(err);
    }
  }
}
