import { Request, Response, NextFunction } from "express";
import AddItemCartService from "../../services/cart/addProduct.service";

interface IProduct {
  id: number;
  name: string;
  price: number;
}

export default class AddItemCartController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid as string;
      const product: IProduct = req.body;

      const additemService = new AddItemCartService();
      const cartResponse = await additemService.execute({ uuid, product });

      return res.status(201).json(cartResponse);
    } catch (err) {
      next(err);
    }
  }
}
