import { Request, Response, NextFunction } from "express";
import RemoveProductService from "../../services/cart/removeProduct.service";
import { protectedReturn } from "../../utils/utils";

export default class RemoveProductController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid as string;
      const isAdm = req.user?.isAdm as boolean;
      const productId = parseInt(req.params.product_id);
      const cartId = parseInt(req.body.cartId);

      const removeProductService = new RemoveProductService();
      const cartResponse = await removeProductService.execute({
        productId,
        cartId,
        uuid,
        isAdm,
      });

      const cartReturn = protectedReturn(cartResponse);
      return res.status(200).json(cartReturn);
    } catch (err) {
      next(err);
    }
  }
}
