import { Request, Response, NextFunction } from "express";
import SelectCartService from "../../services/cart/selectCart.service";

export default class SelectCartController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid as string;
      const isAdm = req.user?.isAdm as boolean;
      const id = parseInt(req.params.id);

      const selectCartService = new SelectCartService();
      const cartResponse = await selectCartService.execute(id, uuid, isAdm);

      return res.status(200).json(cartResponse);
    } catch (err) {
      next(err);
    }
  }
}
