import ListCartsService from "../../services/cart/listCarts.service";
import { Request, Response, NextFunction } from "express";
import { ErrorHandler } from "../../utils/error";

export default class ListCartController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const isAdm = req.user?.isAdm as boolean;
      if (!isAdm) {
        throw new ErrorHandler(401, "Unauthorized");
      }

      const listCartsService = new ListCartsService();
      const cartResponse = await listCartsService.execute();

      return res.status(200).json(cartResponse);
    } catch (err) {
      next(err);
    }
  }
}
