import { Request, Response, NextFunction } from "express";
import { ErrorHandler } from "../../utils/error";
import ListUserService from "../../services/user/listUser.service";

export default class ListUsersController {
  async handle(req: Request, res: Response, next: NextFunction) {
    if (!req.user?.isAdm) {
      return next(new ErrorHandler(401, "Unauthorized"));
    }
    const listUserService = new ListUserService();

    const users = await listUserService.execute();

    return res.status(200).json(users);
  }
}
