import { Request, Response } from "express";
import DeleteUserService from "../../services/user/deleteUser.service";

export default class DeleteUserController {
  async handle(req: Request, res: Response) {
    const { uuid } = req.params;
    const deleUser = new DeleteUserService();
    const message = await deleUser.execute(uuid);

    return res.json({ message: message });
  }
}
