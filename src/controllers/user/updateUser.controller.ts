import { Request, Response, NextFunction } from "express";
import UpdateUserService from "../../services/user/updateUser.service";
import UpdatePasswordService from "../../services/user/changePassword.service";

export default class UpdateUserController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const { uuid } = req.params;
      const { isAdm, ...data } = req.body;

      const updateUserService = new UpdateUserService();

      const user = await updateUserService.execute({ uuid, data });

      return res.status(200).json(user);
    } catch (err) {
      next(err);
    }
  }

  async changePassword(req: Request, res: Response, next: NextFunction) {
    try {
      const uuid = req.user?.uuid;
      const data = { ...req.body, uuid };

      const updatePasswordService = new UpdatePasswordService();

      const newPassword = await updatePasswordService.execute(data);

      return res.status(200).json({ password: newPassword });
    } catch (err) {
      next(err);
    }
  }
}
