import { NextFunction, Response, Request } from "express";
import {
  transport,
  mailTemplateOptions,
} from "../../services/email/email.service";
import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";

import { v4 as uuidv4 } from "uuid";

export default class EmailController {
  async handle(req: Request, res: Response, next: NextFunction) {
    const fromEmail = req.user?.email || "no-repply@mail.com";
    const { to, subject, text } = req.body;

    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne({ email: to });

    if (!user) {
      return next(new ErrorHandler(404, "User not found"));
    }

    const options = mailTemplateOptions(fromEmail, to, subject, "email", {
      user: user.name,
      message: text,
    });

    transport.sendMail(options, function (err, info) {
      if (err) {
        return next(err);
      }
      // else {
      //   console.log(info);
      // }
    });

    return res.status(200).json({ message: "email enviado" });
  }

  async handlePasswordCode(req: Request, res: Response, next: NextFunction) {
    const uuid = req.user?.uuid;
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne({ uuid });

    if (!user) {
      return next(new ErrorHandler(404, "User not found"));
    }

    const newcode = uuidv4();

    await userRepository.update(uuid as string, {
      code: newcode,
    });

    const fromEmail = "no-repply@mail.com";
    const subject = "Mudança de senha";
    const msg = `Seu código para mudança de senha é: ${newcode}`;

    const options = mailTemplateOptions(
      fromEmail,
      user.email,
      subject,
      "email",
      {
        user: user.name,
        message: msg,
      }
    );

    transport.sendMail(options, function (err, info) {
      if (err) {
        return next(err);
      }
      // else {
      //   console.log(info);
      // }
    });
    return res
      .status(200)
      .json({ message: "Código enviado para o email", code: newcode });
  }
}
