import { Request, Response, NextFunction } from "express";
import CreateUserService from "../../services/user/createUser.service";

export default class CreateUserController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const createdUser = new CreateUserService();
      const user = await createdUser.execute(req.body);
      const { password, ...dataWithoutPassword } = user;
      return res.status(201).json(dataWithoutPassword);
    } catch (err) {
      next(err);
    }
  }
}
