import { NextFunction, Response, Request } from "express";
import UserProfileService from "../../services/user/profile.service";
import { ErrorHandler } from "../../utils/error";

export default class UserProfileController {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const uuidToken = req.user?.uuid;
      const uuidParam = req.params.uuid;
      const isAdm = req.user?.isAdm;

      if (uuidToken !== uuidParam && !isAdm) {
        throw new ErrorHandler(401, "Unauthorized");
      }

      const userProfile = new UserProfileService();

      const user = await userProfile.execute(uuidParam as string);

      const { password, ...dataWithoutPassword } = user;

      return res.status(200).json(dataWithoutPassword);
    } catch (e) {
      next(e);
    }
  }
}
