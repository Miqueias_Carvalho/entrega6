import "reflect-metadata";
import app from "./app";
import { createConnection } from "typeorm";

const PORT = process.env.PORT || 3000;

createConnection()
  .then(() => {
    console.log("========================");
    console.log("| Database Connected!! |");
    console.log("========================");

    app.listen(PORT, () => {
      console.log(`sever running at http://localhost:${PORT}`);
    });
  })
  .catch((err) => console.log(err));
