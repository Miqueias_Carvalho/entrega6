import { CartRepository } from "../../repositories/cart.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";

interface IProps {
  productId: number;
  cartId: number;
  isAdm: boolean;
  uuid: string;
}

export default class RemoveProductService {
  async execute({ productId, cartId, uuid, isAdm }: IProps) {
    const cartRepository = getCustomRepository(CartRepository);
    const cart = await cartRepository.findOne({
      where: { id: cartId },
      relations: ["user", "products"],
    });

    if (!cart) {
      throw new ErrorHandler(404, "Cart not Found");
    } else if (cart.user.uuid !== uuid && !isAdm) {
      throw new ErrorHandler(401, "Unauthorized");
    }

    const newProductsList = cart.products.filter((item) => {
      return item.id !== productId;
    });

    cart.products = newProductsList;

    await cartRepository.save(cart);

    return cart;
  }
}
