import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";
import { CartRepository } from "../../repositories/cart.repository";
import { UserRepository } from "../../repositories/user.repository";

import { ProductRepository } from "../../repositories/product.repository";
import { protectedReturn } from "../../utils/utils";

interface IProduct {
  id: number;
  name: string;
  price: number;
}

interface IProps {
  uuid: string;
  product: IProduct;
}

export default class AddItemCartService {
  async execute(data: IProps) {
    const userRepository = getCustomRepository(UserRepository);
    const cartUser = await userRepository.findOne({ uuid: data.uuid });
    if (!cartUser) {
      throw new ErrorHandler(404, "User not found!");
    }

    const productRepository = getCustomRepository(ProductRepository);
    const selectedProduct = await productRepository.findOne({
      id: data.product.id,
    });

    if (!selectedProduct) {
      throw new ErrorHandler(404, "Product not found!");
    }

    const cartRepository = getCustomRepository(CartRepository);
    let cart = await cartRepository.findOne({
      where: { user: cartUser },
      relations: ["products", "user"],
    });

    if (!cart) {
      cart = cartRepository.create({
        user: cartUser,
        products: [selectedProduct],
      });
      await cartRepository.save(cart);
    } else {
      cart.products = [...cart.products, selectedProduct];

      await cartRepository.save(cart);
    }

    const finalPrice = cart.products.reduce((acumulador, item) => {
      return acumulador + item.price;
    }, 0);

    const cartReturn = protectedReturn(cart);
    return { cart: cartReturn, preco_total: finalPrice };
  }
}
