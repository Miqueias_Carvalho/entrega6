import { getCustomRepository } from "typeorm";
import { CartRepository } from "../../repositories/cart.repository";
import { protectedReturn } from "../../utils/utils";

export default class ListCartsService {
  async execute() {
    const cartRepository = getCustomRepository(CartRepository);
    const carts = await cartRepository.find({
      relations: ["products", "user"],
    });

    const cartList = carts.map((item) => {
      const newItem = protectedReturn(item);
      return newItem;
    });

    return cartList;
  }
}
