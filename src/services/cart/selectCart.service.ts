import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";
import { CartRepository } from "../../repositories/cart.repository";
import { protectedReturn } from "../../utils/utils";

export default class SelectCartService {
  async execute(id: number, uuid: string, isAdm: boolean) {
    const cartRepository = getCustomRepository(CartRepository);
    const cart = await cartRepository.findOne({
      where: { id },
      relations: ["products", "user"],
    });

    if (!cart) {
      throw new ErrorHandler(404, "Cart not Found");
    }

    if (cart.user.uuid !== uuid && !isAdm) {
      throw new ErrorHandler(401, "Unauthorized");
    }

    const cartResponse = protectedReturn(cart);

    return cartResponse;
  }
}
