import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";
import { ProductRepository } from "../../repositories/product.repository";

interface IProps {
  name: string;
  price: number;
}

export default class CreateProductService {
  async execute({ name, price }: IProps) {
    const productRepository = getCustomRepository(ProductRepository);

    const productAlreadyExists = await productRepository.findOne({ name });

    if (productAlreadyExists) {
      throw new ErrorHandler(409, "Product already registered");
    }

    const product = productRepository.create({ name, price });

    await productRepository.save(product);

    return product;
  }
}
