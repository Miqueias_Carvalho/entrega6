import { ProductRepository } from "../../repositories/product.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";

export default class OneProductService {
  async execute(id: number) {
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne({ id });

    if (!product) {
      throw new ErrorHandler(404, "Product not found");
    }

    return product;
  }
}
