import { getCustomRepository } from "typeorm";
import { ProductRepository } from "../../repositories/product.repository";

export default class ListProductsService {
  async execute() {
    const productRepository = getCustomRepository(ProductRepository);

    const products = productRepository.find();

    return products;
  }
}
