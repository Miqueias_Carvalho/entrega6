import nodemailer from "nodemailer";
import dotenv from "dotenv";
dotenv.config();
import hbs, {
  NodemailerExpressHandlebarsOptions,
} from "nodemailer-express-handlebars";
import path from "path";

const mailUser = process.env.MAIL_USER;
const mailPass = process.env.MAIL_PASS;

export const transport = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: mailUser,
    pass: mailPass,
    // user: "e152f0e1e7117f",
    // pass: "e71d41149c6343",
  },
});

const handlebarsOption: NodemailerExpressHandlebarsOptions = {
  viewEngine: {
    partialsDir: path.resolve(__dirname, "..", "..", "templates"),
    defaultLayout: undefined,
  },
  viewPath: path.resolve(__dirname, "..", "..", "templates"),
};

transport.use("compile", hbs(handlebarsOption));

export const mailOptions = (
  from: string,
  to: string,
  subject: string,
  text: string
) => {
  return {
    // from: "no-repply@mail.com",
    from,
    to,
    subject,
    text,
  };
};

export const mailTemplateOptions = (
  from: string,
  to: string,
  subject: string,
  template: string,
  context: any
) => {
  return {
    from,
    to,
    subject,
    template,
    context,
  };
};
