import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";

interface IUserProps {
  name: string;
  email: string;
  password: string;
  isAdm: boolean;
}

export default class CreateUserService {
  async execute({ email, name, password, isAdm }: IUserProps) {
    const usersRepository = getCustomRepository(UserRepository);

    const emailAlreadyExists = await usersRepository.findOne({ email });

    if (emailAlreadyExists) {
      throw new ErrorHandler(409, "E-mail already registered");
    }

    const user = usersRepository.create({ name, email, isAdm, password });

    await usersRepository.save(user);

    return user;
  }
}
