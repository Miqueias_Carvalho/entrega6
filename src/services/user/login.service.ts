import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";
import { ErrorHandler } from "../../utils/error";
import jwt from "jsonwebtoken";

export default class LoginService {
  async execute(email: string, password: string) {
    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.findOne({ email });

    if (!user) {
      throw new ErrorHandler(401, "User not registered");
    } else {
      const match = bcrypt.compareSync(password, user.password);
      if (!match) {
        throw new ErrorHandler(401, "User and password missmatch.");
      }
    }

    const token = jwt.sign(
      { uuid: user.uuid, email: user.email },
      process.env.JWT_SECRET as string,
      { expiresIn: "1d" }
    );
    return token;
  }
}
