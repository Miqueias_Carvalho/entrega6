import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";
import { ErrorHandler } from "../../utils/error";

interface Iprops {
  uuid: string;
  password: string;
  code: string;
  confirm: boolean;
}

export default class UpdatePasswordService {
  async execute(data: Iprops) {
    const usersRepository = getCustomRepository(UserRepository);

    const user = await usersRepository.findOne({ uuid: data.uuid });

    if (!user) {
      throw new ErrorHandler(404, "user not found");
    }

    if (data.code !== user.code) {
      throw new ErrorHandler(401, "Invalid Code");
    } else if (!data.confirm) {
      throw new ErrorHandler(401, "Not confirmed");
    }

    const newPassword = bcrypt.hashSync(data.password as string, 10);

    await usersRepository.update(data.uuid, {
      password: newPassword,
    });

    const { password, ...dataWithoutPassword } = user;
    return data.password;
  }
}
