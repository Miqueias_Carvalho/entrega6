import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";

export default class ListUserService {
  async execute() {
    const usersRepository = getCustomRepository(UserRepository);

    const users = await usersRepository.find();
    const userReturn = users.map((item) => {
      const { code, password, ...otherData } = item;
      return otherData;
    });

    return userReturn;
  }
}
