import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import bcrypt from "bcryptjs";
import { ErrorHandler } from "../../utils/error";

interface IUserUpdateProps {
  uuid: string;
  data: {
    name?: string;
    password?: string;
    email?: string;
  };
}

export default class UpdateUserService {
  async execute({ uuid, data }: IUserUpdateProps) {
    const usersRepository = getCustomRepository(UserRepository);

    // if (data.hasOwnProperty("password")) {
    //   data.password = bcrypt.hashSync(data.password as string, 10);
    // }

    const user = await usersRepository.findOne({ uuid });

    if (user) {
      await usersRepository.update(uuid, data);
      const { password, ...dataWithoutPassword } = user;
      return dataWithoutPassword;
    }
    return new ErrorHandler(404, "user not found");
  }
}
