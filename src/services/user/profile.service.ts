import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";

export default class UserProfileService {
  async execute(uuid: string) {
    const userRepository = getCustomRepository(UserRepository);
    try {
      const user = await userRepository.find({ where: { uuid: uuid } });

      const userlist = user.filter((item) => item.uuid === uuid);

      if (userlist.length === 0) {
        throw new ErrorHandler(404, "User not found");
      }

      return userlist[0];
    } catch (e) {
      throw new ErrorHandler((e as any).statusCode || 400, (e as any).message);
    }
  }
}
