import { UserRepository } from "../../repositories/user.repository";
import { getCustomRepository } from "typeorm";

export default class DeleteUserService {
  async execute(uuid: string) {
    const usersRepository = getCustomRepository(UserRepository);
    await usersRepository.delete(uuid);

    return "User deleted with success";
  }
}
