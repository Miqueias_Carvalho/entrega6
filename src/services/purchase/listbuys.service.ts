import { getCustomRepository } from "typeorm";
import { PurchaseRepository } from "../../repositories/purchase.repository";
import { protectedReturn } from "../../utils/utils";

export default class ListBuyService {
  async execute() {
    const buyRepository = getCustomRepository(PurchaseRepository);
    const buy = await buyRepository.find({ relations: ["user", "products"] });

    const list = buy.map((item) => {
      return protectedReturn(item);
    });
    return list;
  }
}
