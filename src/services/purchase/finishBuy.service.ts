import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";
import { PurchaseRepository } from "../../repositories/purchase.repository";
import { CartRepository } from "../../repositories/cart.repository";
import { UserRepository } from "../../repositories/user.repository";

export default class FinishBuyService {
  async execute(uuid: string) {
    const userRepository = getCustomRepository(UserRepository);
    const cartRepository = getCustomRepository(CartRepository);
    const purchaseRepository = getCustomRepository(PurchaseRepository);

    const user = await userRepository.findOne({ uuid });
    if (!user) {
      throw new ErrorHandler(404, "User not Found");
    }

    const cart = await cartRepository.findOne({
      where: { user },
      relations: ["products"],
    });

    if (!cart) {
      throw new ErrorHandler(404, "Cart not Found");
    }

    const data = { user, products: cart.products };
    const buy = purchaseRepository.create(data);

    cart.products = [];

    await purchaseRepository.save(buy);
    await cartRepository.save(cart);

    return buy;
  }
}
