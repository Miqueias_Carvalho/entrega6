import { getCustomRepository } from "typeorm";
import { ErrorHandler } from "../../utils/error";
import { PurchaseRepository } from "../../repositories/purchase.repository";
import { protectedReturn } from "../../utils/utils";

export default class SelectBuyService {
  async execute(buyId: number, uuid: string, isAdm: boolean) {
    const buyRepository = getCustomRepository(PurchaseRepository);
    const buy = await buyRepository.findOne({
      where: { id: buyId },
      relations: ["user", "products"],
    });

    if (!buy) {
      throw new ErrorHandler(404, "Purchase not Found");
    }

    if (buy.user.uuid !== uuid && !isAdm) {
      throw new ErrorHandler(401, "Unauthorized");
    }

    const buyResponse = protectedReturn(buy);
    return buyResponse;
  }
}
