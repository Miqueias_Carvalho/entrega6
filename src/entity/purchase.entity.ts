import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
  CreateDateColumn,
} from "typeorm";

import { Product } from "./product.entity";
import { User } from "./user.entity";

@Entity("purchase")
export class Purchase {
  @PrimaryGeneratedColumn()
  id!: number;

  @CreateDateColumn()
  purchasedOn!: Date;

  @ManyToOne((type) => User, (user) => user.purchases)
  user!: User;

  @ManyToMany((type) => Product)
  @JoinTable()
  products!: Product[];
}
