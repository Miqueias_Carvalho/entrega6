import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";

@Entity("product")
export class Product {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column({ type: "float", default: 0.0 })
  price!: number;
}
