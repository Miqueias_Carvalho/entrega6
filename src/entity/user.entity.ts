import bcrypt from "bcryptjs";
import { v4 as uuidv4 } from "uuid";

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm";
import { Purchase } from "./purchase.entity";

@Entity("user")
export class User {
  @PrimaryGeneratedColumn("uuid")
  uuid!: string;

  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column()
  isAdm!: boolean;

  @CreateDateColumn()
  createdOn!: Date;

  @UpdateDateColumn()
  updatedOn!: Date;

  @Column()
  password!: string;

  @Column()
  code!: string;

  @BeforeInsert()
  Code() {
    this.code = uuidv4();
  }

  @BeforeInsert()
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 10);
  }

  @OneToMany((type) => Purchase, (purchase) => purchase.user)
  purchases!: Purchase[];
}
