import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { User } from "./user.entity";
import { Product } from "./product.entity";

@Entity("cart")
export class Cart {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToMany((type) => Product, { cascade: true })
  @JoinTable()
  products!: Product[];

  @OneToOne((type) => User)
  @JoinColumn()
  user!: User;
}
