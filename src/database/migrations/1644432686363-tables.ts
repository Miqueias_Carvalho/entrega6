import {MigrationInterface, QueryRunner} from "typeorm";

export class tables1644432686363 implements MigrationInterface {
    name = 'tables1644432686363'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "product" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "price" double precision NOT NULL DEFAULT '0', CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "purchase" ("id" SERIAL NOT NULL, "purchasedOn" TIMESTAMP NOT NULL DEFAULT now(), "userUuid" uuid, CONSTRAINT "PK_86cc2ebeb9e17fc9c0774b05f69" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("uuid" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "email" character varying NOT NULL, "isAdm" boolean NOT NULL, "createdOn" TIMESTAMP NOT NULL DEFAULT now(), "updatedOn" TIMESTAMP NOT NULL DEFAULT now(), "password" character varying NOT NULL, "code" character varying NOT NULL, CONSTRAINT "PK_a95e949168be7b7ece1a2382fed" PRIMARY KEY ("uuid"))`);
        await queryRunner.query(`CREATE TABLE "cart" ("id" SERIAL NOT NULL, "userUuid" uuid, CONSTRAINT "REL_10ad03394e9f479b66e4abf6da" UNIQUE ("userUuid"), CONSTRAINT "PK_c524ec48751b9b5bcfbf6e59be7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "purchase_products_product" ("purchaseId" integer NOT NULL, "productId" integer NOT NULL, CONSTRAINT "PK_6dda49c7a99eadd7549d7a71db9" PRIMARY KEY ("purchaseId", "productId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_60f37c2b142964ed7e6c38c32c" ON "purchase_products_product" ("purchaseId") `);
        await queryRunner.query(`CREATE INDEX "IDX_6cdd70374e621d22a3f466eb7a" ON "purchase_products_product" ("productId") `);
        await queryRunner.query(`CREATE TABLE "cart_products_product" ("cartId" integer NOT NULL, "productId" integer NOT NULL, CONSTRAINT "PK_785ab9c1dbede19ef42bf12280b" PRIMARY KEY ("cartId", "productId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_e6ce39be5d354954a88ded1eba" ON "cart_products_product" ("cartId") `);
        await queryRunner.query(`CREATE INDEX "IDX_0fc996e42b6330c97f8cffbddf" ON "cart_products_product" ("productId") `);
        await queryRunner.query(`ALTER TABLE "purchase" ADD CONSTRAINT "FK_84f381a4c1dd32b1a4faadad480" FOREIGN KEY ("userUuid") REFERENCES "user"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "cart" ADD CONSTRAINT "FK_10ad03394e9f479b66e4abf6da9" FOREIGN KEY ("userUuid") REFERENCES "user"("uuid") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "purchase_products_product" ADD CONSTRAINT "FK_60f37c2b142964ed7e6c38c32cf" FOREIGN KEY ("purchaseId") REFERENCES "purchase"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "purchase_products_product" ADD CONSTRAINT "FK_6cdd70374e621d22a3f466eb7ae" FOREIGN KEY ("productId") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "cart_products_product" ADD CONSTRAINT "FK_e6ce39be5d354954a88ded1ebac" FOREIGN KEY ("cartId") REFERENCES "cart"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "cart_products_product" ADD CONSTRAINT "FK_0fc996e42b6330c97f8cffbddfa" FOREIGN KEY ("productId") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "cart_products_product" DROP CONSTRAINT "FK_0fc996e42b6330c97f8cffbddfa"`);
        await queryRunner.query(`ALTER TABLE "cart_products_product" DROP CONSTRAINT "FK_e6ce39be5d354954a88ded1ebac"`);
        await queryRunner.query(`ALTER TABLE "purchase_products_product" DROP CONSTRAINT "FK_6cdd70374e621d22a3f466eb7ae"`);
        await queryRunner.query(`ALTER TABLE "purchase_products_product" DROP CONSTRAINT "FK_60f37c2b142964ed7e6c38c32cf"`);
        await queryRunner.query(`ALTER TABLE "cart" DROP CONSTRAINT "FK_10ad03394e9f479b66e4abf6da9"`);
        await queryRunner.query(`ALTER TABLE "purchase" DROP CONSTRAINT "FK_84f381a4c1dd32b1a4faadad480"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0fc996e42b6330c97f8cffbddf"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_e6ce39be5d354954a88ded1eba"`);
        await queryRunner.query(`DROP TABLE "cart_products_product"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6cdd70374e621d22a3f466eb7a"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_60f37c2b142964ed7e6c38c32c"`);
        await queryRunner.query(`DROP TABLE "purchase_products_product"`);
        await queryRunner.query(`DROP TABLE "cart"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "purchase"`);
        await queryRunner.query(`DROP TABLE "product"`);
    }

}
